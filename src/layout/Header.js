import React from "react"
import "./Header.css"
import { Link } from "react-router-dom"

export default function Header() {
  return (
    <header>
      <div className="container header">
        <div className="logo">LOGO</div>
        <ul className="menu">
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/news">News</Link>
          </li>
        </ul>
      </div>
    </header>
  )
}
