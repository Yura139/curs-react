import React, { useEffect, useState } from "react"
import "./App.css"
import Header from "./layout/Header"
import Home from "./home/Home"
import News from "./news/News"

import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom"
import NewsCard from "./news/NewsCard"
import Context from "./Context"

function App() {
  let newss = [
    {
      id: 1,
      countReaded: 0,
      title: "go snow",
      desc: "lorem impsum lorem impsum lorem impsum lorem impsum lorem impsum",
      img: "https://cms.qz.com/wp-content/uploads/2019/12/AP_19311284358603-1-e1576189932643.jpg",
      visible: true,
    },
    {
      id: 2,
      countReaded: 0,
      title: "go walk",
      desc: "lorem impsum lorem impsum lorem impsum lorem impsum lorem impsum",
      img: "https://i.ndtvimg.com/i/2015-11/walk_625x350_51447922218.jpg",
      visible: true,
    },
    {
      id: 3,
      countReaded: 0,
      title: "go swim",
      desc: "lorem impsum lorem impsum lorem impsum lorem impsum lorem impsum",
      img: "https://i0.wp.com/images-prod.healthline.com/hlcmsresource/images/topic_centers/2019-1/baby_swimming-1200x628-header.jpg",
      visible: true,
    },
  ]

  useEffect(() => {
    // fetch("https://jsonplaceholder.typicode.com/posts")
    //   .then((res) => res.json())
    //   .then((json) => {
    //     setNews(
    //       json.map((post) => {
    //         post["countReaded"] = post.id * 10
    //         post["visible"] = true
    //         post["img"] = ""
    //         post["desc"] = post.body
    //         return post
    //       })
    //     )
    //   })

    setNews(newss)
  }, [])

  const readChange = (id) => {
    setNews(
      news.map((item) => {
        if (item.id === id) {
          item.visible ? item.countReaded++ : item.countReaded--
          item.visible = !item.visible
          return item
        } else {
          return item
        }
      })
    )
  }

  const createNews = (data) => {
    const { title, desc, img, countReaded = 0, visible = false } = data
    setNews(news.concat([{ id: Date.now(), countReaded, title, visible, desc, img }]))
  }

  const [news, setNews] = useState([])
  const removeNews = (id) => {
    setNews(news.filter((item) => item.id !== id))
  }
  return (
    <>
      <Context.Provider value={{ removeNews, createNews, news, readChange }}>
        <Router>
          <Header />

          <Switch>
            <Route path="/home" component={Home} />
            <Route exact path="/news" component={News} />
            <Route path="/news/news-card/:id" component={NewsCard} />

            <Redirect from="" to="home" />
          </Switch>
        </Router>
      </Context.Provider>
    </>
  )
}

export default App
