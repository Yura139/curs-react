import React from "react"
import NewsIten from "./NewsItem"

export default function NewsList(props) {
  return (
    <>
      <ul>
        {props.news.map((item, i) => (
          <li key={i} className="box">
            <NewsIten news={item} readchange={props.changeRead} />
          </li>
        ))}
      </ul>
    </>
  )
}
