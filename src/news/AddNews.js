import React, { useEffect, useState } from "react"
import "./News.css"

export default function AddNews({ onCreateNews }) {
  const [values, setValues] = useState({
    title: "",
    desc: "",
    img: "",
    countReaded: "",
    visible: "",
    formData: "",
  })

  useEffect(() => {
    setValues({ ...values, formData: new FormData() })
  }, [])

  const [typeImg, setTypeImg] = useState("img")
  const { title, desc, img, countReaded, visible, formData } = values
  const headlerInput = (name) => (e) => {
    // const value = name == "img" ? e.target.files[0] : e.target.value
    let value
    if (name === "img") {
      value = typeImg === "img" ? URL.createObjectURL(e.target.files[0]) : e.target.value
    } else {
      value = e.target.value
    }

    formData.set(name, value)
    setValues({ ...values, [name]: value, formData })
  }

  const headlerSubmit = async (e) => {
    e.preventDefault()
    setValues({ ...values, formData: "" })
    await onCreateNews(values)
  }
  return (
    <form onSubmit={headlerSubmit}>
      <div className="form-row">
        <label>title</label>
        <input type="text" value={title} onChange={headlerInput("title")} />
      </div>

      <div className="form-row">
        <label>Description</label>
        <input type="text" value={desc} onChange={headlerInput("desc")} />
      </div>

      <div className="form-row">
        <div className="btns">
          <div className="btn" onClick={() => setTypeImg("url")}>
            url
          </div>
          <div className="btn" onClick={() => setTypeImg("img")}>
            img
          </div>
        </div>
        {typeImg == "img" ? (
          <>
            <label>image</label>
            <input type="file" src={img} onChange={headlerInput("img")} />
          </>
        ) : (
          <>
            <label>Image</label>
            <input type="text" value={img} placeholder="write url" onChange={headlerInput("img")} />
          </>
        )}
      </div>
      <input type="submit" />
    </form>
  )
}
