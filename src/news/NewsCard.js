import React, { useContext } from "react"
import Context from "../Context"

const NewsCard = (props) => {
  const id = props.match.params.id
  const { news } = useContext(Context)
  const newsSingle = news && news.find((item) => item.id === +id)
  console.log(newsSingle)
  return (
    <>
      {newsSingle ? (
        <div className="container">
          <h1>{newsSingle.title}</h1>

          <div className="news-card">
            <div className="count">
              <span>{newsSingle.countReaded}</span>
            </div>
            <div className="img">
              <img src={newsSingle.img} alt="" />
            </div>
            <div className="desc">
              <p>{newsSingle.desc}</p>
            </div>
          </div>
        </div>
      ) : (
        <div>Load.....</div>
      )}
    </>
  )
}
export default NewsCard
