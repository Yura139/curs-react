import React, { useContext } from "react"
import Context from "../Context"
import { Link } from "react-router-dom"

export default function NewsIten({ news, readchange }) {
  // const [read, setRead] = useState(false)
  const { removeNews } = useContext(Context)

  return (
    <div className="item">
      <div className="item-inner">
        <Link to={"/news/news-card/" + news.id}>
          <h2>{news.title}</h2>
        </Link>
        <div className="body">
          <img src={news.img} alt="" />
          <p>{news.desc}</p>
          <div className={news.visible ? "btn" : "btn active"} onClick={() => readchange(news.id)}>
            {news.visible ? "Read" : "Unread"}
          </div>

          <div className="btn delete" onClick={() => removeNews(news.id)}>
            X
          </div>
        </div>
        <div className="meta">
          <span>{news.countReaded}</span>
          <span>{news.visible ? "Unread" : "Read"}</span>
        </div>
      </div>
    </div>
  )
}
