import React, { useContext, useEffect, useState } from "react"
import Context from "../Context"
import AddNews from "./AddNews"
import NewsList from "./NewsList"

export default function News() {
  const { removeNews, createNews, news, readChange } = useContext(Context)
  return (
    <div className="container">
      <h1>News</h1>

      <AddNews onCreateNews={createNews} />

      <NewsList news={news} changeRead={readChange} />
      {news.length === 0 && <p>Новин не має</p>}
    </div>
  )
}
